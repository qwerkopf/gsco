<?php  
$autoresponder = ' 
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Glück, Satt & Co.</title>    
</head>

<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
    <center>
        <table style="padding:30px 10px;background:#F4F4F4;width:100%;font-family:arial" cellpadding="0" cellspacing="0">
                
                <tbody>
                    <tr>
                        <td>
                        
                            <table style="max-width:540px;min-width:320px" align="center" cellspacing="0">
                                <tbody>
                                
                                    <tr>
                                        <td style="background:#fff;border:1px solid #D8D8D8;padding:30px 30px" align="center">
                                        
                                            <table align="center">
                                                <tbody>
                                                
                                                    <tr>
                                                        <td style="border-bottom:1px solid #D8D8D8;color:#666;text-align:center;padding-bottom:30px">
                                                            
                                                            <table style="margin:auto" align="center">
                                                                <tbody>
                                                                    <tr>
                                                                        <td style="color:#006F3D;font-size:22px;font-weight:bold;text-align:center;font-family:arial">
                                                                
                                                                <img src="http://gluecksattundco.de/img/logo.png" width="100"><br><br>
                                                                
                                                                            BESTÄTIGUNG 
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    
                                                    <tr>
                                               <td style="color:#666;padding:15px; padding-bottom:0;font-size:14px;line-height:20px;font-family:arial;text-align:left">
                                    
                                                    <div style="font-style:normal;padding-bottom:15px;font-family:arial;line-height:20px;text-align:left">
                                                        
                                                        <p> Hallo <span style="font-weight:bold;color:#006F3D;font-size:16px">'.$sendername.'</span> <br>
                                                        wir haben folgende Bestellung zur Abholung erhalten.</p>
                                                        
                                                        <p style="border-bottom:1px solid #fff; height:0; color:#666;text-align:center;"></p>
                                                        <p><span style="font-weight:bold;font-size:16px">Name:</span> '.$sendername.'</p>
                                                        <p><span style="font-weight:bold;font-size:16px">Ihre eMail:</span> '.$senderemail.'</p>
                                                        
                                                        <p><span style="font-weight:bold;font-size:16px">Ihre Auswahl:</span> <br/>'.$improve_list.'</p>
                                                        <p><span style="font-weight:bold;font-size:16px;">Ihre Nachricht :</span> </p>
                                                        <p style="margin-bottom:0;"> '.nl2br($sendermessage).' </p>
                                                        <p style="border-bottom:1px solid #D8D8D8; height:0; color:#666;text-align:center;padding:15px 0 0 0"></p>
                                                        <p style="margin-top:20px;">Sie können Ihre Bestellung in unserer Filiale in der Marienstrasse 14, 90402 Nürnberg am Folgetag abholen. Unsere Öffnungszeiten sind wie folgt: Montag bis Freitag von 11 - 18 Uhr & Samstag von 11 - 16 Uhr. Nur  Barzahlung.</p>
                                                        <p><span style="font-weight:bold;font-size:13px">Für Rückfragen oder sonstige Wünsche kontaktieren Sie uns einfach per eMail unter:</span> 
                                                            <span style="color:#006F3D;font-size:13px;">'.$receiver_email.'</span>
                                                        </p>                                                        
                                                        
                                                      </div>
                                                            
                                                        </td>
                                                    </tr>
                                                    
                                                </tbody>
                                            </table>
                                            
                                        </td>
                                    </tr>
                                    
                                   
                                    
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    
                    <tr>
                        <td>
                            <table style="max-width:650px" align="center">
                                
                                <tbody>
                                    <tr>
                                        <td style="color:#b4b4b4;text-align: center;font-size:11px;padding-top:10px;line-height:15px;font-family:arial">
                                            <span style="text-align: center;"><strong>Green Smoothies & Health Food</strong><br>
Montag bis Freitag von 11 - 18 Uhr<br> 
Marienstrasse 14, 90402 Nürnberg<br>
Telefon: 0175/381 42 89 <br>E-Mail: info@gluecksattundco.de <br>
Steuernummer: 240/228/50780 <br> USt-IdNr.: 50 354 679 121<br></span>
                                        </td>
            
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
            </tbody>
        </table>
    </center>
</body>
</html>';
?>