<?php 

	if (!isset($_SESSION)) session_start(); 
	if(!$_POST) exit;
	
	// Enter your name or company name below
	$receiver_name = "Glück, Satt & Co.";
	
	// Enter email address below for receiving the form
	// All Contact messages will be sent there
	$receiver_email = "info@gluecksattundco.de";
	
	// Enter email subject below
	// This will be your message subject
	$msg_subject = "Bestätigung";
	
	$sendername = strip_tags(trim($_POST["sendername"]));	
	$senderemail = strip_tags(trim($_POST["senderemail"]));
	$sendermessage = strip_tags(trim($_POST["sendermessage"]));
    $improve = $_POST["improve"];
	if ($improve[0]!=""){
		$improve_list = implode( '<br/>', $improve);
	}
	
	/*
	========================================
	Start server side validation
	========================================
	*/ 
	$errors = array();
	 //validate name
	if(isset($_POST["sendername"])){
	 
			if (!$sendername) {
				$errors[] = "Bitte geben Sie einen Namen an.";
			} elseif(strlen($sendername) < 2)  {
				$errors[] = "Name muss länger als 2 Zeichen sein.";
			}
	 
	}
	//validate email address
	if(isset($_POST["senderemail"])){
		if (!$senderemail) {
			$errors[] = "Sie müssen eine eMail Adresse angeben.";
		} else if (!validEmail($senderemail)) {
			$errors[] = "Bitte geben Sie eine gültige eMail Adresse an.";
		}
	}
	


	//validate check boxes
	if($improve[0]==''){	
		$errors[] = "Bitte wählen Sie maximal 12 Smoothies aus.";
	}	
	

	
	if ($errors) {
		//Output errors in a list
		$errortext = "";
		foreach ($errors as $error) {
			$errortext .= '<li>'. $error . "</li>";
		}
	
		echo '<div class="alert notification alert-error">Folgende Fehler sind aufgetreten:<br><ul>'. $errortext .'</ul></div>';
	
	} else{
	
		require "PHPMailerAutoload.php";
		require "smartmessage.php";
			
		$mail = new PHPMailer();
		$mail->isSendmail();
		$mail->IsHTML(true);
		$mail->From = $senderemail;
		$mail->CharSet = "UTF-8";
		$mail->FromName = $sendername;
		$mail->Encoding = "base64";
		$mail->Timeout = 200;
		$mail->ContentType = "text/html";
		$mail->addAddress($receiver_email, $receiver_name);
		$mail->Subject = $msg_subject;	
		$mail->Body = $message;
		$mail->AltBody = "Use an HTML compatible email client";
				
		// For multiple email recepients from the form 
		// Simply change recepients from false to true
		// Then enter the recipients email addresses
		// echo $message;
		// echo $autoresponder;
		$recipients = false;
		if($recipients == true){
			$recipients = array(
				"address@example.com" => "Recipient Name",
				"address@example.com" => "Recipient Name",
			);
			
			foreach($recipients as $email => $name){
				$mail->AddBCC($email, $name);
			}	
		}
		
		if($mail->Send()) {
		    require "autoresponder.php";
			$automail = new PHPMailer();
		    $automail->isSendmail();
            $automail->From = $receiver_email;
            $automail->FromName = $receiver_name;
			$automail->isHTML(true);                                 
		    $automail->CharSet = "UTF-8";
		    $automail->Encoding = "base64";
		    $automail->Timeout = 200;
		    $automail->ContentType = "text/html";
            $automail->AddAddress($senderemail, $sendername);
            $automail->Subject = "Bestätigung";
            $automail->Body = $autoresponder;
		    $automail->AltBody = "Use an HTML compatible email client";
			$automail->Send();
			
			echo '<div class="alert notification alert-success"><p>Ihre Bestellung wurde versendet!</p> <p>Bitte prüfen Sie Ihre eMails!</p> </div> ';	
		} else {
			echo '<div class="alert notification alert-error">Es ist ein Fehler aufgetreten.</div> ';
		}
	}
		
	// ultimate email validation function
	function validEmail($senderemail) {
		$isValid = true;
		$atIndex = strrpos($senderemail, "@");
		if (is_bool($atIndex) && !$atIndex) {
			$isValid = false;
		} else {
			$domain = substr($senderemail, $atIndex + 1);
			$local = substr($senderemail, 0, $atIndex);
			$localLen = strlen($local);
			$domainLen = strlen($domain);
			if ($localLen < 1 || $localLen > 64) {
				// local part length exceeded
				$isValid = false;
			} else if ($domainLen < 1 || $domainLen > 255) {
				// domain part length exceeded
				$isValid = false;
			} else if ($local[0] == '.' || $local[$localLen - 1] == '.') {
				// local part starts or ends with '.'
				$isValid = false;
			} else if (preg_match('/\\.\\./', $local)) {
				// local part has two consecutive dots
				$isValid = false;
			} else if (!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain)) {
				// character not valid in domain part
				$isValid = false;
			} else if (preg_match('/\\.\\./', $domain)) {
				// domain part has two consecutive dots
				$isValid = false;
			} else if (!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/', str_replace("\\\\", "", $local))) {
				// character not valid in local part unless
				// local part is quoted
				if (!preg_match('/^"(\\\\"|[^"])+"$/', str_replace("\\\\", "", $local))) {
					$isValid = false;
				}
			}
			if ($isValid && !(checkdnsrr($domain, "MX") || checkdnsrr($domain, "A"))) {
				// domain not found in DNS
				$isValid = false;
			}
		}
		return $isValid;
	}
?>