	$(function() {
			   
				/* @reload captcha
				------------------------------------------- */			   
				function reloadCaptcha(){
					$("#captcha").attr("src","php/captcha.php?r=" + Math.random());
				}
				
				$('.captcode').click(function(e){
					e.preventDefault();
					reloadCaptcha();
				});				
			   
				$( "#smart-form" ).validate({
				
						/* @validation states + elements 
						------------------------------------------- */
						errorClass: "state-error",
						validClass: "state-success",
						errorElement: "em",
						onkeyup: false,
						onclick: false,
						
						/* @validation rules 
						------------------------------------------ */
						rules: {
								sendername: {
										required: true,
										minlength: 2
								},		
								senderemail: {
										required: true,
										email: true
								},
								sendersubject: {
										required: false,
										minlength: 4
								},								
								sendermessage: {
										required: true,
										minlength: 10
								},
								'improve[]':{
										required:true
								}
						},
						
					
						messages:{
								sendername: {
										required: 'Geben Sie Ihren Namen ein',
										minlength: 'Name must be at least 2 characters'
								},				
								senderemail: {
										required: 'Geben Sie Ihre eMail Adresse ein',
										email: 'Bitte geben Sie eine gültige eMail Adresse an'
								},
								sendersubject: {
										required: 'Subject is important',
										minlength: 'Subject must be at least 4 characters'
								},														
								sendermessage: {
										required: 'Bitte geben Sie eine ungefähre Zeit an, wann sie Ihre Smoothies abholen wollen',
										minlength: 'Mittleilung muss mehr als 10 Zeichen haben.'
								},
								'improve[]':{
										required: 'Bitte wählen Sie einen Smootie aus.'
								}
						},

						/* @validation highlighting + error placement  
						---------------------------------------------------- */
						highlight: function(element, errorClass, validClass) {
								$(element).closest('.field').addClass(errorClass).removeClass(validClass);
						},
						unhighlight: function(element, errorClass, validClass) {
								$(element).closest('.field').removeClass(errorClass).addClass(validClass);
						},
						errorPlacement: function(error, element) {
						   if (element.is(":radio") || element.is(":checkbox")) {
									element.closest('.option-group').after(error);
						   } else {
									error.insertAfter(element.parent());
						   }
						},
						
						/* @ajax form submition 
						---------------------------------------------------- */						
						submitHandler:function(form) {
							$(form).ajaxSubmit({
								    target:'.result',			   
									beforeSubmit:function(){ 
											$('.form-footer').addClass('progress');
									},
									error:function(){
											$('.form-footer').removeClass('progress');
									},
									 success:function(){
											$('.form-footer').removeClass('progress');
											$('.alert-success').show().delay(10000).fadeOut();
											$('.field').removeClass("state-error, state-success");
											if( $('.alert-error').length == 0){
												$('#smart-form').resetForm();
												reloadCaptcha();
											}
									 }
							  });
						}
						// end submitHandler:
				});		
		
	});				
    