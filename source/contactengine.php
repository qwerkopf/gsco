<?php

$EmailFrom = "info@gluecksattundco.de";
$EmailTo = "info@gluecksattundco.de";
$Subject = "Bestellbestätigung Glück, Satt & Co.";
$Name = Trim(stripslashes($_POST['Name'])); 
$Tel = Trim(stripslashes($_POST['Tel'])); 
$Email = Trim(stripslashes($_POST['Email'])); 
$Message = Trim(stripslashes($_POST['Message'])); 


// $headers .= 'To: Mary <sebastian.keusch@gmail.com>, Kelly <kelly@example.com>' . "\r\n";
$headers .= 'From: Glück, Satt & Co <info@gluecksattundco.de>' . "\r\n";
$headers .= 'Cc: info@gluecksattundco.de' . "\r\n";
// $headers .= 'Bcc: birthdaycheck@example.com' . "\r\n";

    

if( isset( $_POST['interest'] ) && is_array($_POST['interest']) ) {
$selected_values = '';

foreach( $_POST['interest'] as $value ) {
        $selected_values .= $value . '\n';
}
}

// validation
$validationOK=true;
if (!$validationOK) {
  print "<meta http-equiv=\"refresh\" content=\"0;URL=error.htm\">";
  exit;
}

// prepare email body text
$Body .= "\n";
$Body .= "BESTELLBESTÄTIGUNG";
$Body .= "\n";
$Body .= "\n";


$Body = "";
$Body .= "Name: ";
$Body .= $Name;
$Body .= "\n";

$Body .= "Email: ";
$Body .= $Email;
$Body .= "\n";

$Body .= "Nachricht: ";
$Body .= $Message;
$Body .= "\n";

$Body .= "Ihre Auswahl: ";
$Body .= $selected_values;
$Body .= "\n";

$Body .= "\n";
$Body .= "Sie können Ihre Bestellung in unserer Filiale in der Marienstraße 14, Nürnberg am Folgetag abholen.";
$Body .= "\n";

$Body .= "Von Montag bis Samstag von 11 Uhr bis 20 Uhr.";
$Body .= "\n";
$Body .= "\n";
$Body .= "Für Rückfragen oder sonstige Wünsche kontaktieren Sie uns:";
$Body .= "\n";
$Body .= "Green Smoothies & Health Food";
$Body .= "\n";
$Body .= "Telefon: 0175/381 42 89 / Email: info@glücksattundco.de";
$Body .= "\n";

// send email 
$success = mail($Email, $Subject, $Body, $headers);


// redirect to success page 
if ($success){
  print "<meta http-equiv=\"refresh\" content=\"0;URL=thanks.html\">";
}
else{
  print "<meta http-equiv=\"refresh\" content=\"0;URL=error.html\">";
}
?>